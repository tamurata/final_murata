﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeConstantMove : MonoBehaviour
{
    Rigidbody2D rB2D;

    public float speed = 10;

    public SpriteRenderer spriteRenderer;

    // Start is called before the first frame update
    void Start()
    {
        rB2D = GetComponent<Rigidbody2D>();

        rB2D.velocity = Vector3.right * speed;
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    private void FixedUpdate()
    {
      

        if (rB2D.velocity.x > 0f)
        {
            spriteRenderer.flipX = false;
        }
        else
        if (rB2D.velocity.x < 0f)
        {
            spriteRenderer.flipX = true;
        }
    }

        void OnTriggerEnter2D(Collider2D col)
    {
        if(col.gameObject.tag == "TurningPoint")
        {

            rB2D.velocity = new Vector2(-rB2D.velocity.x, rB2D.velocity.y);
        }
    }
}
